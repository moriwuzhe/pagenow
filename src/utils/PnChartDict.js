const textAligns = [
  {
    label: '自动',
    value: 'auto'
  },
  {
    label: '左对齐',
    value: 'left'
  },
  {
    label: '居中',
    value: 'center'
  },
  {
    label: '右对齐',
    value: 'right'
  }
];

const verticalAligns = [
  {
    label: '自动',
    value: 'auto'
  },
  {
    label: '上',
    value: 'top'
  },
  {
    label: '中',
    value: 'middle'
  },
  {
    label: '下',
    value: 'bottom'
  }
];

const labelPositions = [
  {
    label: 'outside',
    value: 'outside',
    remark: '饼图扇区外侧，通过视觉引导线连到相应的扇区'
  },
  {
    label: 'inside',
    value: 'inside',
    remark: '饼图扇区内部'
  },
  {
    label: 'center',
    value: 'center',
    remark: '在饼图中心位置'
  },
];

const orients = [
  {
    label: '横向',
    value: 'horizontal'
  },
  {
    label: '竖向',
    value: 'vertical'
  }
];

const pieRoseTypes = [
  {
    label: '不设置',
    value: '',
    remark: ''
  },
  {
    label: 'radius',
    value: 'radius',
    remark: '扇区圆心角展现数据的百分比，半径展现数据的大小'
  },
  {
    label: 'area',
    value: 'area',
    remark: '所有扇区圆心角相同，仅通过半径展现数据大小'
  },
];

const rotates = [
  {label: '水平', value: 0},
  {label: '斜角', value: 40},
  {label: '垂直', value: 270}
];

const legendPositions = [
  {label: '顶部居左', value: 'top-left'},
  {label: '顶部居中', value: 'top-center'},
  {label: '顶部居右', value: 'top-right'},
  {label: '底部居左', value: 'bottom-left'},
  {label: '底部居中', value: 'bottom-center'},
  {label: '底部居右', value: 'bottom-right'},
];

const positions = [
  {label: '上', value: 'top'},
  {label: '左', value: 'left'},
  {label: '右', value: 'right'},
  {label: '下', value: 'bottom'},
  {label: '内部', value: 'inside'},
  {label: '内部居左', value: 'insideLeft'},
  {label: '内部居右', value: 'insideRight'},
  {label: '内部居上', value: 'insideTop'},
  {label: '内部居下', value: 'insideBottom'},
  {label: '内部局左上', value: 'insideTopLeft'},
  {label: '内部局左下', value: 'insideBottomLeft'},
  {label: '内部局右上', value: 'insideTopRight'},
  {label: '内部局右下', value: 'insideBottomRight'},
];

const symbols = [
  {label: '圆形', value: 'circle'},
  {label: '空心圆', value: 'emptyCircle'},
  {label: '矩形', value: 'rect'},
  {label: '圆角矩形', value: 'roundRect'},
  {label: '三角形', value: 'triangle'},
  {label: '菱形', value: 'diamond'},
  {label: '别针', value: 'pin'},
  {label: '箭头', value: 'arrow'},
  {label: '空', value: 'none'}
];

const triggers = [
  {label: '数据项', value: 'item'},
  {label: '坐标轴', value: 'axis'}
];

const triggerOns = [
  {label: '悬浮', value: 'mousemove'},
  {label: '点击', value: 'click'}
];

const pieLabelPositions = [
  {label: '饼图扇区外侧', value: 'outside'},
  {label: '饼图扇区内部', value: 'inside'},
  {label: '饼图中心位置', value: 'center'}
];

const axisTypes = [
  {label: '数值型', value: 'value'},
  {label: '类目型', value: 'category'},
  {label: '时间型', value: 'time'},
];

const borderStyles = [
  {
    label: '实线',
    value: 'solid'
  },
  {
    label: '虚线',
    value: 'dashed'
  },
  {
    label: '点线',
    value: 'dotted'
  }
];

const axisPointerTypes = [
  {label: '直线指示器', value: 'line'},
  {label: '阴影指示器', value: 'shadow'},
  {label: '十字准星指示器', value: 'cross'},
  {label: '无', value: 'none'}
];

const scatterLabelPositions = [
  {label: 'top', value: 'top'},
  {label: 'left', value: 'left'},
  {label: 'right', value: 'right'},
  {label: 'bottom', value: 'bottom'},
  {label: 'inside', value: 'inside'},
  {label: 'insideLeft', value: 'insideLeft'},
  {label: 'insideRight', value: 'insideRight'},
  {label: 'insideTop', value: 'insideTop'},
  {label: 'insideBottom', value: 'insideBottom'},
  {label: 'insideTopLeft', value: 'insideTopLeft'},
  {label: 'insideBottomLeft', value: 'insideBottomLeft'},
  {label: 'insideTopRight', value: 'insideTopRight'},
  {label: 'insideBottomRight', value: 'insideBottomRight'}
];

const markLineDataTypes = [
  {label: '最小值', value: 'min'},
  {label: '最大值', value: 'max'},
  {label: '平均值', value: 'average'},
  {label: '中位数', value: 'median'}
];

const markLineLabelPositions = [
  {label: 'start', value: 'start'},
  {label: 'middle', value: 'middle'},
  {label: 'end', value: 'end'},
  {label: 'insideStartTop', value: 'insideStartTop'},
  {label: 'insideStartBottom', value: 'insideStartBottom'},
  {label: 'insideMiddleTop', value: 'insideMiddleTop'},
  {label: 'insideMiddleBottom', value: 'insideMiddleBottom'},
  {label: 'insideEndTop', value: 'insideEndTop'},
  {label: 'insideEndBottom', value: 'insideEndBottom'}
];

const markPointDataTypes = [
  {label: '最小值', value: 'min'},
  {label: '最大值', value: 'max'},
  {label: '平均值', value: 'average'}
];

export default {
  textAligns,
  verticalAligns,
  labelPositions,
  orients,
  pieRoseTypes,
  rotates,
  legendPositions,
  positions,
  symbols,
  triggers,
  triggerOns,
  pieLabelPositions,
  axisTypes,
  borderStyles,
  axisPointerTypes,
  scatterLabelPositions,
  markLineDataTypes,
  markLineLabelPositions,
  markPointDataTypes
}
