
const buildWidgetFieldConfig = function (otherConfig = {}) {
  return Object.assign({
    bindingFieldName: '',
  }, otherConfig)
}

const buildFormItemConfig = function (otherConfig = {}, label = '') {
  return Object.assign({
    label: label,
    labelWidth: 100,
    labelFor: '',
    required: false,
    rules: [],
    showMessage: true,
    hidden: false,
    hiddenLabel: false,
  }, otherConfig)
}

const buildWidgetEventCodeConfig = function (otherConfig = {}) {
  return Object.assign({
    onMounted: {remark: '组件初始化完成后触发', resultValues: [], code: ''}
  }, otherConfig)
}

const buildButtonConfig = function (otherConfig = {}, label = '按钮') {
  return Object.assign({
    type: 'default',
    customClass: 'm-r-5px',
    label: label,
    ghost: false,
    size: 'default',
    shape: 'default',
    long: false,
    disabled: false,
    loading: false,
    icon: '',
    customIcon: '',
    onClick: {remark: '点击按钮时触发', resultValues: '无', code: ''},
  }, otherConfig)
}

export default {
  buildWidgetFieldConfig,
  buildFormItemConfig,
  buildWidgetEventCodeConfig,

  buildButtonConfig
}
