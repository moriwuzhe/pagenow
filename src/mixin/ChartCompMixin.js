
const ChartCompMixin = {
  props: {

  },
  data() {
    return {
      chartDomNum: this.$PnUtil.getTimestamp(),
      chart: null,  // 存储图表实例
    }
  },
  created () {
    // redrawChartRunning变量用于Echart图表重绘时记录使用，主要防止多次运行ds_resultObj的watch，造成多次运行init的问题
    this.$PnDesigner.setRedrawChartRunning('chart_' + this.component.id, '-1')
  },
  mounted () {
    this.initChartAndTheme();
  },
  beforeDestroy () {
    if (this.chart) {
      this.chart.dispose();
    }
  },
  methods: {
    resizeHandle () {
      if (this.chart) {
        if (!this.$store.state.release.pageMetadata) { // 加入这个判断是因为当前Echarts版本在执行resize之后，折线图无法显示初始化动画效果的BUG，目前正在等待echarts官方修复此问题
          this.chart.resize();
        }
      }
    },

    initEchartsInstance () {
      // this.chart = this.$Echarts.init(document.getElementById('chart-'+this.component.id+'-'+this.chartDomNum), 'globalTheme', {
      //   renderer: this.component.compConfigData.renderer ? this.component.compConfigData.renderer : 'canvas'
      // });

      new Promise((resolve, reject)=>{
        let echartDom = document.getElementById('chart-'+this.component.id+'-'+this.chartDomNum)
        // 修复布局块初始阶段为不显示时，造成图表组件渲染尺寸不对的问题
        if (this.layoutItem.layoutItemConfigData.display == 'none') {
          echartDom.style.width = this.layoutItem.layoutItemConfigData.width + 'px'
          echartDom.style.height = this.layoutItem.layoutItemConfigData.height + 'px'
        }
        this.chart = this.$Echarts.init(echartDom, 'globalTheme', {
          renderer: this.component.compConfigData.renderer ? this.component.compConfigData.renderer : 'canvas'
        });
        resolve()
      }).then(()=>{
        this.runEchartsSpreadingCode()
        this.$EventBus.$off('pn_init_datasource_' + this.component.id)
        this.$EventBus.$on('pn_init_datasource_' + this.component.id, () => {
          this.runEchartsSpreadingCode()
        })
      })
    },

    /**
     * 初始化图表实例与主题
     */
    initChartAndTheme () {
      this.$Echarts.registerTheme('globalTheme', this.pageMetadata.theme_json);
      this.initEchartsInstance()

      if (!this.$store.state.release.pageMetadata) {
        this.$EventBus.$off('pn_theme_change_' + 'chart-'+this.component.id+'-'+this.chartDomNum);
        this.$EventBus.$on('pn_theme_change_' + 'chart-'+this.component.id+'-'+this.chartDomNum, (theme_json) => {
          this.$Echarts.registerTheme('globalTheme', theme_json);
          this.chart.dispose();
          this.initEchartsInstance();
          this.chart.setOption(this.chartOption)
        })
      }
    },

    /**
     * 此函数已经弃用，但为了旧版本的兼容性，暂时不删除
     */
    redrawChart () {
      this.$PnDesigner.setRedrawChartRunning('chart_' + this.component.id, '1');
      this.init()
    },

    /**
     * 重绘图表，此函数一般用于运行时动态执行的脚本中使用
     * 如果系统运行时是在Echarts图表组件的初始化运行函数中执行此函数，此函数会覆盖FuncCompMixin内的redrawComp函数
     */
    redrawComp () {
      console.log('执行redrawComp重绘Echarts图表组件，组件ID（' + this.component.id + '）')
      this.$PnDesigner.setRedrawChartRunning('chart_' + this.component.id, '1');
      this.init()
    },

    runDataItemClickJsCode (_callback = () => {}) {
      let _this = this;
      this.chart.off('click');
      this.chart.on('click', (params) => {
        if (typeof _callback === 'function') {
          _callback(params)
        }
        eval(this.component.compConfigData.dataItemClickJsCode)
      })
    },

    /**
     * 运行扩展脚本
     */
    runEchartsSpreadingCode () {
      if (this.component.compConfigData.echartsSpreadingCode) {
        let chart = this.chart
        eval(this.component.compConfigData.echartsSpreadingCode)
      }
    },

    /**
     * 结果集变化后重绘Echarts
     */
    dsResultObjChangeHandler () {
      if (this.$PnDesigner.getRedrawChartRunning('chart_'+this.component.id) == '1') {
        this.$PnDesigner.setRedrawChartRunning('chart_' + this.component.id, '-1')
      }else {
        // 这里判断，当前的图表是否是echarts-gl图表，如果是，先执行clear，解决配置属性修改之后，图表渲染出错的问题
        if (this.component.compConfigData.chartOption.pn_chart_type
          && this.component.compConfigData.chartOption.pn_chart_type == 'echarts-gl') {
          this.chart.clear();
          this.chart.setOption(this.chartOption)
        }else {
          this.chart.setOption(this.chartOption, true)
        }
      }
      this.runEchartsSpreadingCode()
    }
  },
  computed: {

  },
  watch: {
    'component.compConfigData.chartOption': {
      // 此处监听到的val与oldVal是相等的，可能因为chartOption是一个对象
      handler: function (val, oldVal) {
        // 这里判断，当前的图表是否是echarts-gl图表，如果是，先执行clear，解决配置属性修改之后，图表渲染出错的问题
        if (val.pn_chart_type && val.pn_chart_type == 'echarts-gl') {
          // this.chart.clear();
          this.chart.setOption(this.chartOption)
        }else {
          this.chart.setOption(this.chartOption, true)
        }
        this.runEchartsSpreadingCode()
      },
      deep: true
    },
    'component.compConfigData.ds_resultObj': {
      handler: function (val, oldVal) {
        if (JSON.stringify(val) != JSON.stringify(oldVal)) {
          this.dsResultObjChangeHandler()
        }
      },
      deep: true
    },
    'component.compConfigData.ds_resultObjTemplate': {
      handler: function (val, oldVal) {
        if (JSON.stringify(val) != JSON.stringify(oldVal)) {
          this.dsResultObjChangeHandler()
        }
      },
      deep: true
    },
    'component.compConfigData.customOption': {
      handler: function (val, oldVal) {
        if (val != oldVal) {
          // 这里判断，当前的图表是否是echarts-gl图表，如果是，先执行clear，解决配置属性修改之后，图表渲染出错的问题
          if (this.component.compConfigData.chartOption.pn_chart_type
            && this.component.compConfigData.chartOption.pn_chart_type == 'echarts-gl') {
            this.chart.clear();
            this.chart.setOption(this.chartOption)
          }else {
            this.chart.setOption(this.chartOption, true)
          }
          this.runEchartsSpreadingCode()
        }
      },
      deep: true
    },
    'component.compConfigData.ds_filterCode': {
      handler: function (val, oldVal) {
        if (val != oldVal) {
          // 这里判断，当前的图表是否是echarts-gl图表，如果是，先执行clear，解决配置属性修改之后，图表渲染出错的问题
          if (this.component.compConfigData.chartOption.pn_chart_type
            && this.component.compConfigData.chartOption.pn_chart_type == 'echarts-gl') {
            this.chart.clear();
          }
          this.init()
        }
      }
    },
    'component.compConfigData.renderer': {
      handler: function (val, oldVal) {
        if (val != oldVal) {
          this.chart.dispose();
          this.initEchartsInstance();
          this.chart.setOption(this.chartOption)
          this.runEchartsSpreadingCode()
        }
      }
    },
    'component.compConfigData.echartsSpreadingCode': {
      handler: function (val, oldVal) {
        if (val != oldVal) {
          this.runEchartsSpreadingCode()
        }
      }
    }
  }
};

export default ChartCompMixin
