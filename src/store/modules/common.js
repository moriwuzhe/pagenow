
import { getField, updateField } from 'vuex-map-fields';

const state = {
  interactionSourceData: {},
  shareDatasourceResultSet: {}, // 共享数据源结果集
}

const getters = {
  getField,

  getInteractionSourceData (state) {
    return state.interactionSourceData
  },

  getShareDatasourceResultSet (state) {
    return state.shareDatasourceResultSet
  },
  getShareDatasourceResultSetItem: (state) => (key) => {
    if (state.shareDatasourceResultSet[key]) {
      return state.shareDatasourceResultSet[key]
    }else {
      return undefined
    }
  }
}

const mutations = {
  updateField,

  setInteractionSourceData (state, sourceData) {
    state.interactionSourceData = sourceData
  },
  setInteractionSourceDataItem (state, payload) {
    state.interactionSourceData[payload.key] = payload.value
  },

  setShareDatasourceResultSet (state, resultSet) {
    state.shareDatasourceResultSet = resultSet
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}
