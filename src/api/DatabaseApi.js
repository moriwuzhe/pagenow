import {Axios} from '../utils/AxiosPlugin'
import PnDesigner from "../utils/PnDesigner";

const invokeSql = async function (pool_name, sql) {
  sql = PnDesigner.replaceStringSourceInteractionFields(sql);
  return await Axios.post('/database/runSql', {pool_name: pool_name, sql: sql});
};

const getAllDatabase = async function (project_id) {
  return await Axios.get('/database/getAllDatabase', {params: {project_id: project_id}});
};

const getDatabaseByPage = async function (pageIndex, pageSize) {
  return await Axios.post('/database/getDatabaseByPage', {pageIndex: pageIndex, pageSize: pageSize});
};

const addDatabase = async function (addDatabaseForm) {
  return await Axios.post('/database/addDatabase', addDatabaseForm);
};

const removeDatabase = async function (pool_name) {
  return await Axios.delete('/database/removeDatabase', {params: {pool_name: pool_name}});
};

const testDatabaseConn = async function (testDatabaseConnForm) {
  return await Axios.post('/database/testDatabaseConn', testDatabaseConnForm);
}

const checkPoolName = async function (pool_name) {
  return await Axios.get('/database/checkPoolName', {params: {pool_name: pool_name}})
}

export default {
  invokeSql,
  getAllDatabase,
  getDatabaseByPage,
  addDatabase,
  removeDatabase,
  testDatabaseConn,
  checkPoolName
}
