import { Http } from "../utils/HttpPlugin";
import PnDesigner from "../utils/PnDesigner";

import AuthApi from './AuthApi'
import ProjectApi from './ProjectApi'
import PageApi from './PageApi'
import TestApi from './TestApi'
import CompinfoApi from './CompinfoApi'
import EchartThemeApi from './EchartThemeApi'
import ReleaseApi from './ReleaseApi'
import DatabaseApi from './DatabaseApi'
import CsvDatasourceApi from './CsvDatasourceApi'
import FilterApi from './FilterApi'
import UserApi from './UserApi'
import PageTemplateApi from './PageTemplateApi'
import EnshrineCompApi from './EnshrineCompApi'
import HttpProxyApi from './HttpProxyApi'
import CompUtilApi from './CompUtilApi'
import DesignerApi from './DesignerApi'
import FileUploadApi from './FileUploadApi'
import StatisticsApi from './StatisticsApi'
import MapGeoJsonApi from './MapGeoJsonApi'
import PageSnapshootApi from './PageSnapshootApi'
import AttachmentApi from "./AttachmentApi";
import ResourceApi from "./ResourceApi";
import LogLoginApi from "./LogLoginApi";
import LogOperateApi from "./LogOperateApi";
import ShareCustomCompApi from "./ShareCustomCompApi";

/**
 * Axios通过GET请求获取数据
 * @param url
 * @param params
 * @returns {Promise<*>}
 */
const httpGet = async function (url, params) {
  return await Http.get(url, {params: params})
};

/**
 * HTTP请求
 * @param method 请求方式：GET、POST
 * @param apiPath 请求地址
 * @param headers 头信息
 * @param postData POST请求提交数据
 * @returns {Promise<void>}
 */
const httpQuery = async function (method = 'GET', apiPath = '', headers = {}, postData = {}) {
  apiPath = PnDesigner.replaceStringSourceInteractionFields(apiPath);
  if (Object.prototype.toString.call(headers) === '[object String]') {
    try {
      let headersObj = JSON.parse(headers)
      headers = headersObj
    }catch (e) {
      console.error(e)
    }
  }
  if (Object.prototype.toString.call(postData) === '[object String]') {
    try {
      let postDataObj = JSON.parse(postData)
      postData = postDataObj
    }catch (e) {
      console.error(e)
    }
  }
  switch (method) {
    case 'GET':
      return await Http.get(apiPath, {
        headers: PnDesigner.replaceObjectSourceInteractionFields(headers)
      })
    case 'POST':
      return await Http.post(apiPath, PnDesigner.replaceObjectSourceInteractionFields(postData), {
        headers: PnDesigner.replaceObjectSourceInteractionFields(headers)
      })
  }
};

export default {
  httpGet,
  httpQuery,
  AuthApi,
  ProjectApi,
  PageApi,
  TestApi,
  CompinfoApi,
  EchartThemeApi,
  ReleaseApi,
  DatabaseApi,
  CsvDatasourceApi,
  FilterApi,
  UserApi,
  PageTemplateApi,
  EnshrineCompApi,
  HttpProxyApi,
  CompUtilApi,
  DesignerApi,
  FileUploadApi,
  StatisticsApi,
  MapGeoJsonApi,
  PageSnapshootApi,
  AttachmentApi,
  ResourceApi,
  LogLoginApi,
  LogOperateApi,
  ShareCustomCompApi
}
