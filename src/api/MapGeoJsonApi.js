import {Axios} from '../utils/AxiosPlugin'

const saveMapGeoJson = async function (mapGeoJson) {
  return await Axios.post('/mapGeoJson/saveMapGeoJson', mapGeoJson);
};

const getMapGeoJsonByPage = async function (pageIndex, pageSize, adcode, level, alias_name) {
  return await Axios.post('/mapGeoJson/getMapGeoJsonByPage', {pageIndex: pageIndex, pageSize: pageSize, adcode: adcode, level: level, alias_name: alias_name});
};

const getMapGeoJsonById = async function (id) {
  return await Axios.get('/mapGeoJson/getMapGeoJsonById', {params: {id: id}});
}

const getGeoJsonByAdCode = async function (adcode) {
  return await Axios.get('/mapGeoJson/getGeoJsonByAdCode', {params: {adcode: adcode}});
};

const getMapBaseInfoByLevel = async function (levels) {
  return await Axios.get('/mapGeoJson/getMapBaseInfoByLevel', {params: {levels: levels}});
};

const getAdCodeByAliasName = async function (aliasName) {
  return await Axios.get('/mapGeoJson/getAdCodeByAliasName', {params: {aliasName: aliasName}});
};

const deleteMapGeoJson = async function (id) {
  return await Axios.delete('/mapGeoJson/deleteMapGeoJson', {params: {id: id}});
};

export default {
  saveMapGeoJson,
  getMapGeoJsonByPage,
  getMapGeoJsonById,
  getGeoJsonByAdCode,
  getMapBaseInfoByLevel,
  getAdCodeByAliasName,
  deleteMapGeoJson
}
