import {Axios} from '../utils/AxiosPlugin'

const getDesignerData = async function (pageId) {
  return await Axios.get('/designer/getDesignerData', {params: {pageId: pageId}});
};

export default {
  getDesignerData
}
