# 实例

## this

指向当前组件的vm实例

## this.chart

Echart图表组件特有，指向图表容器实例

# 全局对象

## $

全局JQuery实例对象

## EventBus

全局事件总线对象

| 函数名  | 描述     | 参数                                                         |
| ------- | -------- | ------------------------------------------------------------ |
| send    | 发送事件 | name：事件名称<br />payload：发送数据                        |
| receive | 接收事件 | names：事件名称，可以是一个字符串数组<br />callBack：回调函数，必须使用剪头函数 |

## Echarts

全局Echarts对象

## PnUtil

全局通用工具类

| 函数名          | 描述                 | 函数入参                                                     |
| --------------- | -------------------- | ------------------------------------------------------------ |
| uuid            | 生成UUID             | 无                                                           |
| getTimestamp    | 获取当前时间戳       | 无                                                           |
| getRootPath     | 获取项目工程根路径   | 无                                                           |
| jumpLink        | 跳转链接             | href：链接地址<br />target：页面打开方式                     |
| openIframeModal | 打开一个模态窗       | src：链接地址<br />width：窗口宽度，字符串类型<br />height：窗口高度，字符串类型<br />scrolling：是否显示滚动条，布尔类型，默认auto |
| deepClone       | 深拷贝对象           | obj：被拷贝对象                                              |
| addUrlParams    | 动态添加URL参数      | key：键值<br />value：数值                                   |
| deleteUrlParams | 动态删除URL参数      | name：参数键值名称                                           |
| getUrlParam     | 获取URL参数值        | name：参数键值名称                                           |
| deepMerge       | 深合并对象           | obj1：对象1<br />obj2：对象2                                 |
| getCurrentUser  | 获取当前登录用户信息 | 无                                                           |

## PnApi

全局HTTP工具类

| 函数名                 | 描述               | 函数入参                                                     |
| ---------------------- | ------------------ | ------------------------------------------------------------ |
| httpQuery              | 客户端HTTP请求     | method：请求方式-GET和POST<br />apiPath：请求地址<br />headers：头信息<br />postData：POST请求提交数据 |
| HttpProxyApi.httpQuery | 服务器代理HTTP请求 | method：请求方式-GET和POST<br />apiPath：请求地址<br />headers：头信息<br />postData：POST请求提交数据 |

## 全局变量
| 变量名          | 描述                 |
| --------------- | -------------------- |
| BASE_PATH            | 项目工程根路径，读取形式：http://xxx/xxx             |
