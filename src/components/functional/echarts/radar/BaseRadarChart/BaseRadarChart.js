/**
 * 构建系列配置数据
 * @param name 系列名称
 * @returns
 */
const buildSeriesObj = function (name) {
  return {
    name: name,
    symbolSize: 2.5,
    itemStyle: {
      color: ''
    },
    label: { // 单个拐点文本的样式设置
      normal: {
        show: false, // 单个拐点文本的样式设置。[ default: false ]
        position: 'top', // 标签的位置。[ default: top ]
        color: '#fff', // 文字的颜色。如果设置为 'auto'，则为视觉映射得到的颜色，如系列色。[ default: "#fff" ]
        fontSize: 12
      }
    },
    areaStyle: {
      normal: {
        startColor: '',
        endColor: '',
      }
    }
  }
};

export default {
  buildSeriesObj
}
