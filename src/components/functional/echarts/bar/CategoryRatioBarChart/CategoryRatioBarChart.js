const buildSeriesObj = function (color = '') {
  return {
    type: 'bar',
    barWidth: 16,
    stack: '1',
    label: {
      formatter: "(params) => {\n\treturn params.value\n}",
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: color,
    }
  }
}

const buildDefaultSeriesObj = function (name = '', color = '', value) {
  return {
    name: name,
    type: 'bar',
    barWidth: 16,
    stack: '1',
    label: {
      borderWidth: 10,
      distance: 20,
      align: 'center',
      verticalAlign: 'middle',
      borderRadius: 1,
      borderColor: color,
      backgroundColor: color,
      show: true,
      position: 'top',
      formatter: '{c}%',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: color
    },
    data: [
      {
        value: value
      }
    ]
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
