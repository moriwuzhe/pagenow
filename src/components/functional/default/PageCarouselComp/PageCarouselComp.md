# this.swiper

在组件内部脚本中（初始化运行脚本）或同页面下其他组件内部脚本中通过findCompVmById获取到当前页面轮播组件的VM实例后，
组件暴露对外的swiper属性对象可以对当前页面轮播组件进行一系列的API操作

Swiper更为详细的API文档，可前往 [https://www.swiper.com.cn/](https://www.swiper.com.cn/) 查看

## Swiper常用方法

1、滑动到下一个滑块
```javascript
/**
 * speed	num	可选	切换速度(单位ms)
 * runCallbacks	boolean	可选	设置为false时不会触发transition回调函数
 */
this.swiper.slideNext(speed, runCallbacks)
```

2、滑动到前一个滑块
```javascript
/**
 * speed	num	可选	切换速度(单位ms)
 * runCallbacks	boolean	可选	设置为false时不会触发transition回调函数
 */
this.swiper.slidePrev(speed, runCallbacks)
```

3、切换到指定slide
```javascript
/**
 * index	num	必选	指定将要切换到的slide的索引，索引从1开始
 * speed	num	可选	切换速度(单位ms)
 * runCallbacks	boolean	可选	设置为false时不会触发transition回调函数
 */
this.swiper.slideTo(index, speed, runCallbacks)
```
