const buildColumnMappingItem = function (columnField = '', columnName = '') {
  return {
    columnField: columnField,
    columnName: columnName, // 列显示名，用于表头显示
    columnWidth: 0, // 列宽
    textAlign: 'left',
    fontSize: 14,
    textColor: '#fff'
  }
}

export default {
  buildColumnMappingItem
}
