# this.calendar

在组件内部脚本中（初始化运行脚本或选中时触发脚本）或同页面下其他组件内部脚本中通过findCompVmById获取到当前页面轮播组件的VM实例后，
组件暴露对外的calendar属性对象可以对日历组件进行一系列的API操作

## calendar常用方法

1、设置选中当天日期

```javascript
this.calendar.setToday()
```

2、如何指定设置日期

在任何可执行脚本中，通过给组件的`this.value`直接设值，可以指定日历组件选中某个日期，`this.value`的值必须为一个数组，其中包含年、月、日的值

```javascript
/**
 * 单选模式示例
 */
this.value = [2022,2,22]
/**
 * 多选模式示例
 */
this.value = [[2022,2,22], [2022, 2,23]]
```
