import PnUtil from '@/utils/PnUtil'

const buildHiddenObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CHiddenBuilder',
    type: 'hidden',
    title: '隐藏域',
    field: 'field_' + PnUtil.getTimestamp(),
    value: '',
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildInputObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CInputBuilder',
    type: "input",
    title: '文本框',
    field: 'field_' + PnUtil.getTimestamp(),
    value: "",
    props: {
      "type": "text",
      //输入框类型，可选值为 text、password、textarea、url、email、date
      "clearable": false,
      //是否显示清空按钮
      "disabled": false,
      //设置输入框为禁用状态
      "readonly": false,
      //设置输入框为只读
      "rows": 4,
      //文本域默认行数，仅在 textarea 类型下有效
      "autosize": false,
      //自适应内容高度，仅在 textarea 类型下有效，可传入对象，如 { minRows: 2, maxRows: 6 }
      "number": false,
      //将用户的输入转换为 Number 类型
      "placeholder": "请输入...",
      //占位文本
      "size": "default",
      //输入框尺寸，可选值为large、small、default或者不设置
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildSwitchObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CSwitchBuilder',
    type: "switch",
    title: "开关",
    field: 'field_' + PnUtil.getTimestamp(),
    value: "1",
    props: {
      "size": "default",
      //开关的尺寸，可选值为large、small、default或者不写。建议开关如果使用了2个汉字的文字，使用 large。
      "disabled": false,
      //禁用开关
      "trueValue": "1",
      //选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用
      "falseValue": "0",
      //没有选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用
      "slot": {
        open: "开",
        close: "关",
      },
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildCheckboxObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CCheckboxBuilder',
    type: "checkbox",
    title: "多选框",
    field: 'field_' + PnUtil.getTimestamp(),
    value: ["1", "2"],
    options: [
      {
        value: "1",
        label: "苹果",
        disabled: false
      },
      {
        value: "2",
        label: "雪梨",
        disabled: false
      },
      {
        value: "3",
        label: "香蕉",
        disabled: false
      }
    ],
    props: {
      "size": "default",
      //多选框组的尺寸，可选值为 large、small、default 或者不设置
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildRadioObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CRadioBuilder',
    type: "radio",
    title: "单选框",
    field: 'field_' + PnUtil.getTimestamp(),
    value: "0",
    options: [
      {
        value: "0",
        label: "不包邮",
        disabled: false
      },
      {
        value: "1",
        label: "包邮",
        disabled: false
      },
      {
        value: "1",
        label: "未知",
        disabled: false
      },
    ],
    props: {
      "type": 'undefined',
      //可选值为 button 或不填，为 button 时使用按钮样式
      "size": "default",
      //单选框的尺寸，可选值为 large、small、default 或者不设置
      "vertical": false,
      //是否垂直排列，按钮样式下无效
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildSelectObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CSelectBuilder',
    type: "select",
    title: "选择器",
    field: 'field_' + PnUtil.getTimestamp(),
    value: ["1", "2"],
    props: {
      "multiple": true,
      //是否支持多选
      "clearable": false,
      //是否可以清空选项，只在单选时有效
      "filterable": true,
      //是否支持搜索
      "size": "default",
      //选择框大小，可选值为large、small、default或者不填
      "placeholder": "请选择",
      //选择框默认文字
      "not-found-text": "无匹配数据",
      //当下拉列表为空时显示的内容
      "placement": "bottom",
      //弹窗的展开方向，可选值为 bottom 和 top
      "disabled": false,
      //是否禁用
    },
    options: [
      {
        "value": "1",
        "label": "生态蔬菜",
        "disabled": false
      },
      {
        "value": "2",
        "label": "新鲜水果",
        "disabled": false
      },
    ],
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildDatePickerObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CDatePickerBuilder',
    type: "DatePicker",
    title: "日期选择",
    field: 'field_' + PnUtil.getTimestamp(),
    value: [],
    // value: ['2018-02-20', new Date()],
    // input值, type为daterange,datetimerange value为数组 [start_value,end_value]
    props: {
      "type": "date",
      //显示类型，可选值为 date、daterange、datetime、datetimerange、year、month
      "format": "yyyy-MM-dd HH:mm:ss",
      //展示的日期格式
      "placement": "bottom-start",
      //日期选择器出现的位置，可选值为toptop-starttop-endbottombottom-startbottom-endleftleft-startleft-endrightright-startright-end
      "placeholder": "请选择...",
      //占位文本
      "confirm": false,
      //是否显示底部控制栏，开启后，选择完日期，选择器不会主动关闭，需用户确认后才可关闭
      "size": "default",
      //尺寸，可选值为large、small、default或者不设置
      "disabled": false,
      //是否禁用选择器
      "clearable": true,
      //是否显示清除按钮
      "readonly": false,
      //完全只读，开启后不会弹出选择器
      "editable": false,
      //文本框是否可以输入
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildTimePickerObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CTimePickerBuilder',
    type: "TimePicker",
    title: "时间选择",
    field: 'field_' + PnUtil.getTimestamp(),
    value: '',
    props: {
      "type": "time",
      //显示类型，可选值为 time、timerange
      "format": "HH:mm:ss",
      //展示的时间格式
      "steps": [],
      //下拉列表的时间间隔，数组的三项分别对应小时、分钟、秒。例如设置为 [1, 15] 时，分钟会显示：00、15、30、45。
      "placement": "bottom-start",
      //	时间选择器出现的位置，可选值为toptop-starttop-endbottombottom-startbottom-endleftleft-startleft-endrightright-startright-end
      "placeholder": "请选择...",
      //占位文本
      "confirm": false,
      //是否显示底部控制栏，开启后，选择完日期，选择器不会主动关闭，需用户确认后才可关闭
      "size": "default",
      //尺寸，可选值为large、small、default或者不设置
      "disabled": false,
      //是否禁用选择器
      "clearable": true,
      //是否显示清除按钮
      "readonly": false,
      //完全只读，开启后不会弹出选择器
      "editable": false,
      //文本框是否可以输入
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildInputNumberObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CInputNumberBuilder',
    type: "InputNumber",
    title: "数字输入框",
    field: 'field_' + PnUtil.getTimestamp(),
    value: 0,
    //input值
    props: {
      "max": 1000,
      //最大值
      "min": 0,
      //最小值
      "step": 1,
      //每次改变的步伐，可以是小数
      "size": "default",
      //输入框尺寸，可选值为large、small、default或者不填
      "disabled": false,
      //设置禁用状态
      "readonly": false,
      //是否设置为只读
      "editable": true,
      //是否可编辑
      "precision": 0,
      //数值精度
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildColorPickerObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CColorPickerBuilder',
    type: "ColorPicker",
    title: "颜色",
    field: "color",
    value: '',
    props: {
      "alpha": false,
      //是否支持透明度选择
      "hue": true,
      //是否支持色彩选择
      "recommend": false,
      //是否显示推荐的颜色预设
      "size": "default",
      //尺寸，可选值为large、small、default或者不设置
      "colors": [],
      //自定义颜色预设
      "format": "hex",
      //颜色的格式，可选值为 hsl、hsv、hex、rgb,开启 alpha 时为 rgb，其它为 hex
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildRateObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CRateBuilder',
    type: "rate",
    title: "评分器",
    field: 'field_' + PnUtil.getTimestamp(),
    value: 3.5,
    props: {
      "count": 10,
      //star 总数
      "allowHalf": true,
      //是否允许半选
      "disabled": false,
      //是否只读，无法进行交互
      "showText": true,
      //是否显示提示文字
      "clearable": true,
      //是否可以取消选择
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildSliderObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CSliderBuilder',
    type: "slider",
    title: "滑块",
    field: 'field_' + PnUtil.getTimestamp(),
    value: [0, 50],
    //滑块选定的值。普通模式下，数据格式为数字，在双滑块模式下，数据格式为长度是2的数组，且每项都为数字
    props: {
      "min": 0,
      //最小值
      "max": 100,
      //最大值
      "step": 1,
      //步长，取值建议能被（max - min）整除
      "disabled": false,
      //是否禁用滑块
      "range": false,
      //是否开启双滑块模式
      "showInput": false,
      //是否显示数字输入框，仅在单滑块模式下有效
      "showStops": false,
      //是否显示间断点，建议在 step 不密集时使用
      "showTip": "hover",
      //提示的显示控制，可选值为 hover（悬停，默认）、always（总是可见）、never（不可见）
      "tipFormat": undefined,
      //Slider 会把当前值传给 tip-format，并在 Tooltip 中显示 tip-format 的返回值，若为 null，则隐藏 Tooltip
      "inputSize": "small",
      //数字输入框的尺寸，可选值为large、small、default或者不填，仅在开启 show-input 时有效
    },
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

const buildButtonObj = function () {
  return {
    builderId: PnUtil.uuid(),
    builderName: 'CButtonBuilder',
    type: "i-button",
    props:{
      type: 'primary',
      ghost: false,
      size: 'default',
      long: false,
      loading: false,
      icon: '',
      to: '',
      target: '_self'
    },
    children: ['自定义按钮'],
    customJsCode: '',
    validate: [],
    col: {
      span: 24,
      offset: 0
    }
  }
};

export default {
  buildHiddenObj,
  buildInputObj,
  buildSwitchObj,
  buildCheckboxObj,
  buildRadioObj,
  buildSelectObj,
  buildDatePickerObj,
  buildTimePickerObj,
  buildInputNumberObj,
  buildColorPickerObj,
  buildRateObj,
  buildSliderObj,
  buildButtonObj
}
