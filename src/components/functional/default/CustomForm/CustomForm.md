## 交互

### 初始化运行脚本

初始化运行脚本中，我们可以执行一些表单初始化完成之后需要执行的操作，例如设置表单值等等，在此脚本中，可以直接使用this获取当前组件的vm对象，当然也可以使用所有相关的全局变量

### 获取表单实例对象

在【自定义表单提交逻辑】中，通过【_this】获取当前自定义表单组件的vm对象，通过如下代码获取表单的实例对象，通过此实例对象，可以使用表单的相关API来操作表单

```javascript
_this.fApi
```

#### 字段操作

- 获取表单所有字段名

  ```javascript
  _this.fApi.fields()
  ```

- 获取表单的键值对

  ```javascript
  _this.fApi.formData()
  ```

- 获取指定字段的值

  ```javascript
  _this.fApi.getValue(field)
  ```

- 设置字段的值

  ```javascript
  // 设置单个值
  _this.fApi.setValue(field,value)
  // 批量设置
  _this.fApi.setValue({
    field1:value1,
    field2:value2,
    field3:value3
  });
  ```

- 删除指定字段

  ```javascript
  _this.fApi.removeField(field)
  ```

#### 表单操作

- 表单验证

  ```javascript
  _this.fApi.validate((valid)=>{
    if(valid){
      //TODO 验证通过
    }else{
      //TODO 验证未通过
    }
  })
  // 验证指定字段
  _this.fApi.validateField(field,(errMsg)=>{
    if(errMsg){
      //TODO 验证未通过
    }else{
      //TODO 验证通过
    }
  });
  ```

- 重置表单

  ```javascript
  _this.fApi.resetFields()
  ```

- 清除组件的验证信息

  ```
  _this.fApi.clearValidateState()
  _this.fApi.clearValidateState(field)
  _this.fApi.clearValidateState([field1,field2])
  ```

- 修改表单提交按钮规则

  ```javascript
  _this.fApi.submitBtnProps({loading:true})
  ```

### 如何提交表单数据

在【自定义表单提交逻辑】中，只能通过axios插件来执行表单的提交操作，如果对axios不了解的，请自行百度进行学习。

#### 通过formData获取表单数据

在【自定义表单提交逻辑】中，通过变量【formData】可以直接获取当前表单的数据对象

#### 提交表单数据

下面为提交表单的一个简单的示例代码：

```javascript
_this.$axios.post('http://localhost:8090/test/testPost', formData, {
  headers: {
    'Content-Type': 'application/json;charset=UTF-8'
  }
}).then((result)=>{
  _this.$Message.success(result.data);
  _this.fApi.resetFields()
})
```

此代码中【_this.$axios】为平台封装好的Axios对象，直接使用即可。

#### 接收表单数据

后端接收代码示例，以SpringBoot为例

```java
@RequestMapping("/testPost")
public String testPost (@RequestBody JSONObject jsonParam) {
  System.out.println(jsonParam.toJSONString());
  return "success";
}
```

本示例中需要添加如下JAR工具依赖：

```xml
<dependency>
	<groupId>com.alibaba</groupId>
	<artifactId>fastjson</artifactId>
	<version>1.2.49</version>
</dependency>
```

#### 跨域问题

由于使用的是axios进行表单的数据提交，可能会涉及到跨域的问题，这里我们建议在后端接收程序中，使用CORS作为解决跨域问题的方案，具体配置方案请自行百度。
