const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const join = require('path').join;
const bodyParser = require('body-parser');
const cors = require('cors');
const port = 3325;

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/**
 * 获取目录下所有文件名
 * @param dir
 * @param files_
 * @returns {*|*[]}
 */
function getFiles (dir, files_){
  files_ = files_ || [];
  let files = fs.readdirSync(dir);
  for (let i in files){
    let name = dir + '/' + files[i];
    if (fs.statSync(name).isDirectory()){
      getFiles(name, files_);
    } else {
      files_.push(name);
    }
  }
  return files_;
}

app.post('/createCompFiles', (req, res) => {

  let componentName = req.body.componentName;
  let componentAliasName = req.body.componentAliasName;
  let componentType = req.body.componentType;
  let rootFolder = req.body.rootFolder;

  // 组件主文件模板路径
  let mainTemplateFilePath = '';
  // 组件配置表单模板路径
  let formTemplateFilePath = '';

  // 判断添加的组件文件是什么类型
  if (componentType == 'general') { // 普通功能组件
    mainTemplateFilePath = './src/template/Template.vue';
    formTemplateFilePath = './src/template/TemplateForm.vue'
  }else if (componentType == 'echarts') { // Echarts图表组件
    mainTemplateFilePath = './src/template/ChartTemplate.vue';
    formTemplateFilePath = './src/template/ChartTemplateForm.vue'
  }else if (componentType == 'echarts3D') { // Echarts3D图表组件
    mainTemplateFilePath = './src/template/Chart3DTemplate.vue';
    formTemplateFilePath = './src/template/ChartTemplateForm.vue'
  }

  // 读取主文件模板文件
  let mainTemplate = fs.readFileSync(path.join(__dirname, mainTemplateFilePath), 'utf8');
  // 读取配置表单模板文件
  let formTemplate = fs.readFileSync(path.join(__dirname, formTemplateFilePath), 'utf8');

  // 替换主模板文件中的componentName字符串为组件的名称
  let mainTemplateContent = mainTemplate.replace(/ComponentName/g, componentName).replace(/componentAliasName/g, componentAliasName);
  // 替换配置表单模板文件中的componentName字符串为组件的名称，特殊处理需要加上Form字符
  let formTemplateContent = formTemplate.replace(/ComponentName/g, componentName);

  // 组件文件夹目录路径，例如组件名为TestComp，那么就会创建一个名为TextComp的文件夹用于存放组件相关的文件
  let targetDirPath = path.join(__dirname, './src/components/functional/' + rootFolder, componentName);
  // 创建的组件的主文件路径
  let targetMainTemplateFilePath = path.join(__dirname, './src/components/functional/' + rootFolder, componentName, componentName + '.vue');
  // 创建的组件的配置表单文件路径
  let targetFormTemplateFilePath = path.join(__dirname, './src/components/functional/' + rootFolder, componentName, (componentName + 'Form') + '.vue');


  // 创建组件文件夹目录
  if (!fs.existsSync(targetDirPath)) {
    fs.mkdirSync(targetDirPath);
    console.log('目录（'+targetDirPath+'）创建完成！');
  }

  // 创建组件主文件
  if (!fs.existsSync(targetMainTemplateFilePath)) {
    fs.writeFile(targetMainTemplateFilePath, mainTemplateContent, (err) => {
      if (err) throw err;
      console.log('组件（' + targetMainTemplateFilePath + '）文件创建完成！');
    });
  } else {
    console.error('组件（' + targetMainTemplateFilePath + '）文件已存在！');
  }

  // 创建组件配置表单文件
  if (!fs.existsSync(targetFormTemplateFilePath)) {
    fs.writeFile(targetFormTemplateFilePath, formTemplateContent, (err) => {
      if (err) throw err;
      console.log('组件（'+targetFormTemplateFilePath+'）文件创建完成！');
    });
  } else {
    console.error('组件（'+targetFormTemplateFilePath+'）文件已存在！');
  }

  res.json({
    success: true,
    message: '组件相关文件创建完成'
  });

});

app.listen(port, () => console.log(`开发环境工具库启动成功，运行端口号（${port}）。`));
